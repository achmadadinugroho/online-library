<?php

        session_start(); 

		$level = $_SESSION['level'];

$username = $_SESSION['username'];

//Pindah ke Form Login Jika belum Login

if(empty($_SESSION['username'])){
	
	header('location:../Index.php');
	
}
else{
	


?>

<!DOCTYPE HTML>
<html>

	<!-----HEADER------>

<head>
	<link href="../Config/Template.css" type="text/css" rel="stylesheet">
	<link href="../Config/Dropdown.css" type="text/css" rel="stylesheet">
	<link href="../Config/Tamu.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" type="text/css">
	<title>Form Upload Pdf</title>

</head>
<body>
		<div class="header">
		<table class="header">
		
		<tr>
			<td>
				<img class="logo" src="Images/logo-baznas.png">
			</td>
			
			<td>
				<h1>ONLINE LIBRARY</h1>
			</td>
			
			<td class="search">
			
			<form>
				<input class="search" type="text" placeholder="Search Pdf.." name="search">
				<input class="searchbutton" type="button" value="Search">	
			</form>	
				
			</td>
			
			</tr>
		
		</table>
		</div>
		
		<div class="menu-wrap">
	<ul>
		
		<li><a href="Home.php">Home</a></li>
		<li><a href="Library.php">Library</a></li>
		
		<li class="Drop2"><a>Admin <i class=\"fa fa-angle-down\"></i></a>
			<ul>
				<li><a href="Upload.php">Upload PDF</a></li>
				<li><a href="Signup.php">Add User</a></li>
			</ul>
		</li>
		<li class="Drop"><a><img class="Drop" src="Images/Dropdown.png"></a>
			<ul>
				<li><a><?php echo $username; ?></a></li>
				<li><a href="Index.Php?Logout=true">Log Out</a></li>
			</ul>
		</li>
		
	</ul>
		</div>
		
	<!-----CLOSE HEADER------>	
	
	
	
			<!-----BODY------->	
	<div class="chest">
	
	<div class="form">
	
	<h3>File Upload Form</h3>
	<hr>
	<form method="post" action="Upload.php" enctype="multipart/form-data">
	<table class="form" class="upload" style="font-family:calibri">
	
	<tr><th>Content</th> <td><input type="file" id="file" accept="application/pdf" name="userfile" class="inputfile"></td></tr>

	<tr><th>Title</th> <td><input type="text" name="judul">&nbsp; .PDF<br>* Kosongkan Title untukmenggunakan default name.</td></tr>
	
	<tr><th>Type</th> <td>
	
	<select name="tipe">
		
		<option value="Informasi Sertifikasi">Informasi Sertifikasi</option>
		
		<option value="Peraturan & Pedoman">Peraturan & Pedoman</option>
	
	</select>
	</td></tr>
	
	
	
	<tr><th>Description</th> <td><textarea name="description" rows=4 cols=30 maxlength="160"></textarea></td></tr>
	
	<tr><td>&nbsp;</td><td><button type="submit" name="upload">Upload</button></td></tr>
	</table>
	</form>
	</div>
	
	</div>
			<!-----CLOSE BODY------>
			
			
	<!-----Footer------>
	<div class="footer">
	<div class="footer2">
	<p class="copyright">Copyright  &copy; <?php echo date('Y'); ?> Online - Library by Asharisan. Alrights Reserved.
	</p>
	</div>
	<ul>
		
		<li><a href="Home.php">Home</a></li>
		<li><a href="Library.php">Library</a>
			
		</li>
		<li><a href="#">Go Top</a>
			
		</li>
		
		
	</ul>
	</div>
<html>

<?php
include "../Controller/Upload-controller.php";

upload_controller($username);


	}
	
?>




