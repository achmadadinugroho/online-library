
<?php

        session_start(); 

$level = $_SESSION['level'];
	
$username = $_SESSION['username'];

//Pindah ke Form Login Jika belum Login
if(empty($_SESSION['username'])){
	header('location:Index.php');
}

include "../Controller/Library-controller.php";
?>

<!DOCTYPE HTML>
<html>

	<!-----HEADER------>

<head>
	<link href="../Config/Library_Template.css" type="text/css" rel="stylesheet">
	<link href="../Config/Dropdown.css" type="text/css" rel="stylesheet">
	<link href="../Config/Library.css" rel="Stylesheet" type="text/css">
	<link href="../Config/Paging.css" rel="Stylesheet" type="text/css">
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" type="text/css">
	<title>Library</title>

</head>
<body>
		<div class="header">
		<table class="header">
		
		<tr>
			<td>
				<img class="logo" src="Images/logo-baznas.png">
			</td>
			
			<td>
				<h1>ONLINE LIBRARY</h1>
			</td>
			
			<td class="search">
			
			<form action="Library.php">
				<input class="search" type="text" placeholder="Search Pdf.." name="search">
				<input class="searchbutton" type="submit" value="Search">	
			</form>	
				
			</td>
			
			</tr>
		
		</table>
		</div>
		
		<div class="menu-wrap">
	<ul>
		
		<li><a href="Home.php">Home</a></li>
		<li><a href="Library.php">Library</a></li>
		
		<?php 
		
		//jika Admin maka muncul menu Admin
		
		if($level=='1'){
		echo "
		<li class=\"Drop2\"><a>Admin <i class=\"fa fa-angle-down\"></i></a>
			<ul>
				<li><a href=\"Upload.php\">Upload PDF</a></li>
				<li><a href=\"Signup.php\">Add User</a></li>
				
			</ul>
		</li>";
		}
		else{
			echo "";
		}
		?>
		<li class="Drop"><a><img class="Drop" src="Images/Dropdown.png"></a>
			<ul>
				<li><a ><?php echo $username; ?></a></li>
				<li><a href="Index.Php?Logout=true">Log Out</a></li>
			</ul>
		</li>
		
	</ul>
		</div>
		
	<!-----CLOSE HEADER------>	
	
	
	
			<!-----BODY------->	
	<div class="chest">
	
	
<?php library_controller($level); ?>
	
	
	</div>
			<!-----CLOSE BODY------>
			
			
	<!-----Footer------>
	<div class="footer">
	<div class="footer2">
	<p class="copyright">Copyright  &copy; <?php echo date('Y'); ?> Online - Library by Asharisan. Alrights Reserved.
	</p>
	</div>
	<ul>
		
		<li><a href="Home.php">Home</a></li>
		<li><a href="Library.php">Library</a>
			
		</li>
		<li><a href="#">Go Top</a>
			
		</li>
		
		
	</ul>
	</div>
</html>