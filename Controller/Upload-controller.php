
<?php

function upload_controller($username){
	
include_once '../Config/Koneksi.php';

include "../Model/Upload-model.php";

//Jika sebuah file erupload

if(isset($_POST['upload']) && $_FILES['userfile']['size'] > 0)
{
$pdf = ".pdf";	

$judul  =	$_POST['judul'].$pdf;

$description  =	$_POST['description'];

$tipe  =	$_POST['tipe'];
	
$fileName = $_FILES['userfile']['name'];

$tmpName  = $_FILES['userfile']['tmp_name'];

$fileSize = $_FILES['userfile']['size'];

$fileType = $_FILES['userfile']['type'];

//Jika Title kosong maka menggunakan nama default file

if(empty($_POST['judul'])){
	$newname = $fileName;
}
else{
	$newname = $judul;
}

$tgl = date('Y-m-d');

$allowedExts = array(
  "pdf", 
  "doc", 
  "docx"
); 

$allowedMimeTypes = array( 
  'application/msword',
  'text/pdf',
  'image/gif',
  'image/jpeg',
  'image/png'
);

$extension = end(explode(".", $newname));

//Batas maksimal size file

if ( 3000000 < $fileSize  ) {
  die( 'Please provide a smaller file [E/1].' );
}

if ( ! ( in_array($extension, $allowedExts ) ) ) {
  die('Please provide another file type [E/2].');
}



if(!get_magic_quotes_gpc())
{
    $newname = addslashes($newname);
}

 Upload_Model($newname, $fileSize, $fileType, $tgl, $username,$description, $tipe, $konek);

$uploads_dir = "../../Uploads";

 
$success = move_uploaded_file($tmpName, "$uploads_dir/$newname");

//Muncul Message bila sukses

if($success){
	echo "<script>alert(\"Upload Success!\")</script>";
}
else{
	echo "<script>alert(\"Upload Failure!\")</script>";
}
	}
else{
	echo "Proses tidak berjalan!";
}

}
?>