<?php


//Fungsi Yang Mengatur Library.php

function library_controller($level) {
	
include "../Model/Library-model.php"; //memasukan file Library-model.php
	
$batas 		= 5;

$halaman 	= @$_GET['halaman'];

//Apabila $halaman kosong

if(empty($halaman)){
	
$posisi 	= 0;

$halaman 	= 1;
}
else {
$posisi = ($halaman-1) * $batas;
}
$file = $_SERVER['PHP_SELF'];

//Menampilkan Table bagian atas

echo "
<div class=\"short_by\">
	<ul>
		<li><a>Short &nbsp;<i class=\"fa fa-angle-down\"></i></a>
			<ul >
				<li> <a href=\"$file?halaman=1&short=all\">All</a></li>
				<li> <a href=\"$file?halaman=1&short=pedoman\">Peraturan & Pedoman</a></li>
				<li> <a href=\"$file?halaman=1&short=informasi\">Informasi Sertifikasi</a></li>
			</ul>
		</li>
	</ul>
</div>

<table class=\"pusat\">



<tr><td colspan=\"6\"><hr></td></tr>

<tr><th width=\"30px\">No.</th><th>&nbsp;</th><th class=\"title\">Title</th></tr>

<tr><td colspan=\"6\"><hr></td></tr>
";



//Jika search data
if(isset($_GET['search'])){
	$search = $_GET['search'];
	
	$hasil = search($search,$posisi,$batas);
	$jmldata = search_page($search);

}
else
{	
//Jika diurutkan berdasarkan
	if(isset($_GET['short'])){
		
		$short = $_GET['short'];
		
		$hasil = shortby($short,$posisi,$batas);
		$jmldata = pages($short);
	}
	else{
		
		$short = 'all';
		
		$hasil = default_query($posisi,$batas);
		$jmldata = default_query2();
	}
}

$no =$posisi + 1;

//Pengulangan Array data Pdf
while($data = fetch_array($hasil))
{
	if (strlen($data['deskripsi']) > 105) {
		
	$trimstring = substr($data['deskripsi'], 0, 100). ' <a href="Library.php?id='.$data['id'].'">readmore...</a>';
} 

	else {
	$trimstring = $data['deskripsi'];
}
	
	echo "	<tr>
				<td rowspan=\"4\">$no</td>
				<td rowspan=\"4\" class=\"icon\">
					<img src=\"Images/pdf.png\" height=\"60px\" width=\"60px\">
				</td>
				<th  rowspan=\"2\" >
					$data[nama_file]
				</th>
				<td colspan=\"2\">$data[tipe]</td>
				<td rowspan=\"2\">";
					
					//Jika Level adalah Admin maka terdapat menu Edit dan Delete
					if($level==1){
					echo"
					<form method=\"get\" action=\"Edit.php\">
					
						<input type=\"hidden\" name=\"edit\" value=\"".$data['id']."\">
						<button class=\"float2\" type=\"submit\" ><img title=\"Edit\" src=\"Images/edit-pdf.png\" class=\"png\"></button>
					
					</form>
				
					<form method=\"get\" action=\"../Controller/Library-controller.php\">
						<input type=\"hidden\" name=\"delete\" value=\"".$data['nama_file']."\">
						<input type=\"hidden\" name=\"delete_id\" value=\"".$data['id']."\">
						<button class=\"float\" type=\"submit\" ><img title=\"Delete\" src=\"Images/delete-pdf.png\" class=\"png\"></button>
					";
					}
					else {
						echo "";
					}
					echo "
					
					</form>
				</td>
			</tr>
			
			<tr>
			</tr>
			
			<tr>
				<td rowspan=2 class=\"title\">
				";
				//Jika diclick Readmore
				if(isset($_GET['id'])){
					echo $data['deskripsi'];
				}
				else {
					echo $trimstring;
				}	
				echo	"
				</td>
				<td>
				Admin <td>: $data[username]</td>
				</td>
				<td rowspan=\"2\" width=\"100px\">

					
						<li class=\"show\"><a class=\"red\" href=\"../Controller/Library-controller.php?Show=".$data['id']."\">Show<a></li>
					

				

					
				</td>
				
			</tr>
			<tr>
				
				<td>
				Size <td> :&nbsp; ".byteFormat($data['size'],'KB','0')."</td>
				</td>
				
			</tr>
			<tr><td colspan=6><hr></td></tr>
			";
$no++;

}

echo "</table>";

//Menghitung semua data berdasarkan short



$jmlhalaman =ceil($jmldata/$batas);



echo "<div class=\"paging\">";
//Jika $halaman lebih dari 1, terdapat menu prev
if($halaman > 1){
	
$prev=$halaman-1;

echo "<span class=\"prevnext\">

<a href=\"$file?halaman=$prev&short=$short\"><< Prev</a>

</span> ";


}

else{
echo "<span class=disabled><< Prev</span> ";
}

//Pengulangan For untuk angka dalam paging

for($i=1;$i<=$jmlhalaman;$i++)
if ($i != $halaman) {
echo "<a href=\"$file?halaman=$i&short=$short\">$i</a> ";
}
else{
echo "<span class=\"current\">$i</span> ";
}

//Jika $halaman lebih kecil dari $jumlah halaman, terdapat menu next
if($halaman < $jmlhalaman){
$next=$halaman+1;
echo "<span class=\"prevnext\">
<a href=\"$file?halaman=$next&short=$short\">Next >></a>
</span> ";
}
else{
echo "<span class=disabled>Next >></span> ";
}

echo "</div>";

  echo "<p class=\"absolute\">Jumlah File: <b>$jmldata</b> File</p>";

}

//Script Jika Gambar Show diklik
if(isset($_GET['Show'])){
	$nama = $_GET['Show'];
	show($nama);
}


//Script Jika Button Delete diklik
if(isset($_GET['delete'])){
	
	
	$nama = $_GET['delete'];
	$del = $_GET['delete_id'];
	delete($nama,$del);

}
else {
	echo "";
}

//-----------------------------------------------------------------
//Fungsi untuk menghapus file
function delete($files,$del) {
	include "../Model/Library-model.php";
	
	$delete = execute($del);

	if($delete){
		
	unlink('../../Uploads/'.$files);

	header('location:../View/Library.php');
	
	}
	else{
		echo "WARNING! Delete Failed!";
	}

}


//Fungsi untuk menampilkan file
function show($id){

include "../Model/Library-model.php";

$nama = show_query($id);

$path = '../../Uploads/'.$nama;

$filename = $nama;

$file = $path;

  header('Content-type: application/pdf');
  header('Content-Disposition: inline; filename="' . $filename . '"');
  header('Content-Transfer-Encoding: binary');
  header('Accept-Ranges: bytes');
  @readfile($file);
}


//Fungsi untuk mengatur satuan size
function byteFormat($bytes, $unit = "", $decimals = 2) {
 $units = array('B' => 0, 'KB' => 1, 'MB' => 2, 'GB' => 3, 'TB' => 4, 
 'PB' => 5, 'EB' => 6, 'ZB' => 7, 'YB' => 8);
 
 $value = 0;
 if ($bytes > 0) {
 // Generate automatic prefix by bytes 
 // If wrong prefix given
 if (!array_key_exists($unit, $units)) {
 $pow = floor(log($bytes)/log(1024));
 $unit = array_search($pow, $units);
 }
 
 // Calculate byte value by prefix
 $value = ($bytes/pow(1024,floor($units[$unit])));
 }
 
 // If decimals is not numeric or decimals is less than 0 
 // then set default value
 if (!is_numeric($decimals) || $decimals < 0) {
 $decimals = 2;
 }
 
 // Format output
 return sprintf('%.' . $decimals . 'f '.$unit, $value);
  }


?>